import React from 'react';
import Icon1 from '../../images/undraw_finance_re_gnv2.svg';
import Icon2 from '../../images/undraw_vault_re_s4my.svg';
import Icon3 from '../../images/undraw_online_transactions_-02-ka.svg';
import { ServicesContainer, ServicesH1, ServicesCard, ServicesWrapper, ServicesIcon, ServicesH2, ServicesP  } from './ServicesElements';

const Services = () => {
  return (
    <ServicesContainer id="services">
        <ServicesH1>Our Services</ServicesH1>
        <ServicesWrapper>
            <ServicesCard>
                <ServicesIcon src={Icon1}></ServicesIcon>
                <ServicesH2>Reduce Expenses</ServicesH2>
                <ServicesP>We Help reduce your fess and increase your overall revenue.</ServicesP>
            </ServicesCard>
            <ServicesCard>
                <ServicesIcon src={Icon2}></ServicesIcon>
                <ServicesH2>Your Virtual Offices</ServicesH2>
                <ServicesP>You can access our platform online anywhere in the world.</ServicesP>
            </ServicesCard>
            <ServicesCard>
                <ServicesIcon src={Icon3}></ServicesIcon>
                <ServicesH2>Your Premium Benefits</ServicesH2>
                <ServicesP>Unclock our special membership card that returns 5% cash back.</ServicesP>
            </ServicesCard>
        </ServicesWrapper>


    </ServicesContainer>
  )
}

export default Services