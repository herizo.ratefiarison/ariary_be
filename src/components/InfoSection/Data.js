import Image1 from '../../images/undraw_finance_re_gnv2.svg';
import Image2 from '../../images/undraw_savings_re_eq4w.svg';
import Image3 from '../../images/undraw_online_transactions_-02-ka.svg';
export const homeObjOne = {
    id: 'about',
    lightBg: false,
    lightText: true,
    lightTextDesc: true,
    topLine: 'Premium Bank',
    headLine: 'Unlimited Transactions with zero fees',
    description: 'Get access to our exclusive app that allows you to send enlimited transactions without gettind charged any fees.',
    buttonLabel: 'Get Started',
    imgStart: false,
    img: Image1,
    alt: 'Premium bank',
    dark: true,
    primary: true,
    darkText: false,

};

export const homeObjTwo = {
    id: 'discover',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Unlimited Access',
    headLine: 'Login to your account at any time',
    description: 'We have you covered no matter where you are located. All you need is an internet connection and a phone or computer',
    buttonLabel: 'Learn More',
    imgStart: true,
    img: Image2,
    alt: 'unlimited access',
    dark: false,
    primary: false,
    darkText: true,

};
export const homeObjThree = {
    id: 'signup',
    lightBg: true,
    lightText: false,
    lightTextDesc: false,
    topLine: 'Join our Team',
    headLine: 'Creating an account is extremely easy',
    description: "Get everything set up and ready in under 10 minutes. All you need to do is add your information and you're ready to go",
    buttonLabel: 'Start Now',
    imgStart: false,
    img: Image3,
    alt: 'Join our team',
    dark: false,
    primary: false,
    darkText: true,

};